# syntax=docker/dockerfile:1
FROM python:3.10.0b3-buster
WORKDIR /app
RUN pip3 install requests markdown feedgen
RUN apt-get update && apt-get install -y cron
COPY scraper.py .
VOLUME /data
VOLUME /config
COPY *.jpg  /data/
COPY cronjob /etc/cron.d/zetland2rss
RUN crontab /etc/cron.d/zetland2rss
ENTRYPOINT "crond"
CMD ["-f", "-d", "8"]
