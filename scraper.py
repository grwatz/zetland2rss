import requests
from feedgen.feed import FeedGenerator
import markdown

with open('token', encoding='utf-8') as f:
    token = f.read().splitlines()[0]
headers = {
        # this one was stolen from my logged in browser session,
        # in the http headers for page.json
        "authorization": token
        }

s = requests.Session()
s.headers.update(headers)
md = markdown.markdown
zetland_image_host = "https://zetland.imgix.net/"
page_url = "https://api.zetland.dk/api/v1/consume/stories/page"
readlater_url= ""

def generate_feed(url, id, title, subtitle, type='text', filters=None):
    fg = FeedGenerator()
    fg.load_extension('podcast')

    def get_stories(stories, type):
        for story in stories:
            fe = fg.add_entry()
            # TODO: there are more than one authers sometimes
            author = story['author']['author_content']
            author_name = author['firstname'] + ' ' + author['lastname']
            title = story['story_content']['content']['title']
            if 'subtitle' in story['story_content']['content']:
                subtitle = story['story_content']['content']['subtitle']
            else:
                subtitle = ''
            if 'audio_description' in story['story_content']['content']:
                audiodesc = story['story_content']['content']['audio_description']
            else:
                audiodesc = ''
            blocks = story['story_content']['content']['body']['blocks']
            # story_notes = story['story_content']['content']['body']['footnotes']
            story_markdown = []
            for block in blocks:
                if block['type'] == 'advtext':
                    story_markdown += [block['data']['text']]
                elif block['type'] == 'image':
                    img_markdown  = '\n\n!['
                    img_markdown += block['data']['credit']
                    img_markdown += ']('
                    img_markdown += zetland_image_host
                    img_markdown += block['data']['image']['url']
                    img_markdown += ' "'
                    img_markdown += block['data']['caption']
                    img_markdown += '")\n\n'
                    story_markdown += [img_markdown]
            fe.title(title + ' ' + subtitle)
            fe.id(story['story_id'])
            fe.author({'name': author_name})
            fe.updated(story['published_at'])
            fe.link(href = story['url'], rel = 'alternate')
            if type == 'text':
                fe.summary(story['subhead'])
                fe.content(content = md('\n'.join(story_markdown)),type='CDATA')
            elif type == 'audio':
                fe.description(audiodesc)
                fe.enclosure(url=story['story_content']['meta']['audioFiles'][0])

    def filters_to_query(filters):
        q = ','.join(filters)
        return '?' + q

    def filters_to_json(filters):
        j = 'filter[story_type][]='.join(filters)
        return '?filter[story_type][]=' + j

    if filters:
        p = s.get(url + filters_to_json(filters))
        fg.link(href='http://zetland.dk/historier' + filters_to_query(filters), rel='alternate')
    else:
        p = s.get(url)
        fg.link(href='http://zetland.dk/historier', rel='alternate')
    news = p.json()
    fg.id(id)
    fg.title(title)
    fg.subtitle(subtitle)
    fg.updated(news['stories'][0]['published_at'])
    fg.author({'name': 'Zetlands redaktion', 'email': 'skriv@zetland.dk'})
    fg.category(term='Nyheder')
    fg.icon('./icon.jpg')
    fg.logo('./logoorange.jpg')
    fg.rights('')
    fg.link(href='feeds.andersens.xyz/' + id + '.atom', rel='self')
    fg.language('dk')
    get_stories(news['stories'], type)
    fg.atom_file(id + '.atom')

# All stories, as text
# this requires no filters
generate_feed(
        url=page_url, 
        id='zetland-all-text',
        title='Zetland - alle artikler',
        subtitle='Alle artikler fra Zetland.dk'
        )

# Morning news overview podcast
# this requires some filterin
generate_feed(
        url=page_url, 
        id='zetland-helikopter-audio',
        title='Zetland - Helikopter (podcast)',
        subtitle='Morgenoverblikket fra Zetland.dk',
        filters=['helikopter'],
        type='audio'
        )

